<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/
$user = 'u17333';
$pass = '7038049';
$db = new PDO('mysql:host=localhost;dbname=u17333',$user, $pass, array(PDO::ATTR_PERSISTENT => true));
$query = $db->prepare('SELECT * FROM admin WHERE login = ? AND pass = ?');
$query->execute([
  $_SERVER['PHP_AUTH_USER'],
  md5($_SERVER['PHP_AUTH_PW'])
]
);
$row = $query->fetchAll();


// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    empty($row)) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}


if($_SERVER['REQUEST_METHOD'] == 'GET'){

  include('table.php');

} else {
  $stmt = $db->prepare('DELETE FROM user WHERE id = ?');
  $stmt->execute([$_POST['id']]);
  header('Location: admin.php');
}

?>
